part of 'main.dart';

///
///Init all the dependancy of the project in the container
///
Future<void> setupDepency() async {
  var preference = await SharedPreferences.getInstance();
  Get.put<SharedPreferences>(preference, permanent: true);
  Get.lazyPut<LocalStorage>(() => LocalStorage(), fenix: true);
  Get.lazyPut<Dio>(() => _getDio(), fenix: true);
}

Dio _getDio() {
  // LocalStorage box = Get.find<LocalStorage>();
  var dio = Dio();
  var timezone = DateTime.now().timeZoneOffset;
  var headers = {
    'Accept': 'application/json',
    'offset-timezone': ((timezone.isNegative ? -1 : 1) * timezone.inHours).toString(),
    'locale': Get.locale?.languageCode
  };

  dio.options =
      BaseOptions(baseUrl: Config.baseUrl, headers: headers, sendTimeout: 300000, receiveTimeout: 300000, connectTimeout: 300000);

  dio.interceptors.add(InterceptorsWrapper(
    onRequest: (options, handle) {
      // final token = box.read(Config.keys.tokenkey);
      // if (token != null) {
      //   // print(user!.toJson());
      //   options.headers['Authorization'] = "Bearer $token";
      // }
      // options.headers['language'] = Get.locale!.languageCode;

      // return handle.next(options);
    },
    onResponse: (response, handle) {
      if (!(response.data is Map || response.data is List)) {
        response.data = null;
      }
      return handle.next(response);
    },
    onError: (error, handle) {
      if (error.response != null) {
        switch (error.response!.statusCode) {
          case 500:
            break;
          case 401:
            break;
          case 404:
            break;

          case 422:
            break;
        }
      }
      return handle.next(error);
    },
  ));

  return dio;
}
