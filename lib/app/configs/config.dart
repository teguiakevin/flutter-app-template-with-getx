import 'dart:ui';

import 'package:sizer/sizer.dart';

class Config {
  static final String baseUrl = "https://";
  static final fonts = _Fonts();
  static final keys = _Keys();
  static final colors = _Colors();
  static final sizes = _Sizes();
}

class _Fonts {
  final String montteras = 'assets/fonts/monteras.tt';
}

class _Keys {
  final String currentUser = 'current_user';
}

class _Colors {
  final primaryColor = Color(0xFF346E8C);
}

class _Assets {
  final logo = 'assets/icons/logo.png';
}

class _Sizes {
  final defaultPadding = 3.82.w;
  final heighttPadding = 40.0;
  final s5w = 1.57.w;
  final s5h = 1.05.h;
  final s6w = 2.05.w;
  final s6h = 1.09.h;
  final s8w = 2.5.w;
  final s8h = 1.65.h;
  final s9w = 2.82.w;
  final s9h = 1.859.h;
  final s10w = 3.15.w;
  final s10h = 2.1.h;
  final s12w = 3.8.w;
  final s12h = 2.5.h;
  final s13w = 3.82.w;
  final s13h = 3.82.h;
  final s14w = 3.82.w;
  final s14h = 3.82.h;
  final s15w = 4.7.w;
  final s15h = 3.1.h;
  final s16w = 3.82.w;
  final s16h = 3.82.h;
  final s18w = 3.82.w;
  final s18h = 3.82.h;
  final s20w = 6.25.w;
  final s20h = 4.15.h;
  final s25w = 7.82.w;
  final s25h = 5.155.h;
  final s30w = 9.5.w;
  final s30h = 6.2.h;
  final s35w = 10.95.w;
  final s35h = 7.25.h;
  final s40w = 12.5.w;
  final s40h = 8.25.h;
  final s50w = 15.65.w;
  final s50h = 10.31.h;
  final s70w = 21.9.w;
  final s70h = 14.5.h;
  final s80w = 25.w;
  final s80h = 16.5.h;
  final s150w = 46.9.w;
  final s150h = 31.1.h;
  final s170w = 53.2.w;
  final s170h = 35.1.h;

  final s90w = 28.15.w;
  final s90h = 18.55.h;

  final s200w = 62.5.w;
  final s200h = 41.25.h;

  final s220w = 68.75.w;
  final s220h = 10.31.h;

  //polices

  final p8 = 7.5.sp;
  final p9 = 8.45.sp;
  final p10 = 9.4.sp;
  final p11 = 10.4.sp;
  final p12 = 11.3.sp;
  final p13 = 12.19.sp;
  final p14 = 13.2.sp;
  final p15 = 14.1.sp;
  final p16 = 15.sp;
  final p17 = 16.sp;
  final p18 = 16.9.sp;
  final p19 = 17.9.sp;

  final p20 = 18.75.sp;
  final p22 = 20.7.sp;
  final p25 = 23.5.sp;
  final p30 = 28.4.sp;
}
