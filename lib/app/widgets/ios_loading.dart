import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class IosLoading extends StatelessWidget {
  const IosLoading({Key? key, this.color, this.elevation}) : super(key: key);
  final Color? color;
  final double? elevation;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 100,
      child: Card(
        elevation: elevation,
        color: color ?? Colors.white.withOpacity(.8),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Theme(
          data: ThemeData(cupertinoOverrideTheme: const CupertinoThemeData(brightness: Brightness.light)),
          child: const CupertinoActivityIndicator(
            radius: 30,
          ),
        ),
      ),
    );
  }
}
