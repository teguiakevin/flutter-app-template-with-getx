// ignore_for_file: curly_braces_in_flow_control_structures

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

typedef FutureList = List<Future> Function();

class SplashScreen extends StatefulWidget {
  final Color backgroundColor = Colors.white;
  final TextStyle styleTextUnderTheLoader = const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.black);
  final FutureList? initialFutures;
  final Widget child;

  final String? redirectAfterPlashPageRouteName;
  final Widget? redirectAfterPlashPageRouteWidgetError;
  SplashScreen(
      {required this.child,
      required this.redirectAfterPlashPageRouteName,
      super.key,
      this.initialFutures,
      this.redirectAfterPlashPageRouteWidgetError});

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final splashDelay = 1;

  @override
  void initState() {
    super.initState();
    _loadWidget();
  }

  Future<Timer?> _loadWidget() async {
    var beginAt = DateTime.now();
    Duration duration;
    if (widget.initialFutures != null) {
      try {
        await Future.wait(widget.initialFutures!(), eagerError: true);
        duration = _getDuration(beginAt);
        return Timer(duration, navigationPage);
      } catch (e) {
        duration = _getDuration(beginAt);
        if (widget.redirectAfterPlashPageRouteWidgetError != null) {
          return Timer(duration, () {
            Get.to(widget.redirectAfterPlashPageRouteWidgetError);
          });
        } else {
          return Timer(duration, navigationPage);
        }
      }
    }
  }

  Duration _getDuration(DateTime beginAt) {
    var second = DateTime.now().difference(beginAt).inSeconds;
    if (second > splashDelay) {
      return const Duration(milliseconds: 100);
    } else {
      return Duration(seconds: splashDelay - second, milliseconds: 300);
    }
  }

  void navigationPage() {
    print('next page');
    Get.offNamed(widget.redirectAfterPlashPageRouteName!);
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
