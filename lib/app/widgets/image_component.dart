import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

class ImageComponent extends StatelessWidget {
  final String? url;
  final BorderRadius? borderRadius;
  final double? width;
  final double? height;
  final double iconSize;
  final Widget? itemFav;
  final bool? visibleCount;
  final bool? visibleDot;
  final ScrollPhysics? physic;
  final Function()? onTap;
  final BoxFit? fit;
  final File? file;
  const ImageComponent({
    super.key,
    this.url,
    this.borderRadius,
    this.width,
    this.height,
    this.itemFav,
    this.visibleCount = false,
    this.visibleDot = true,
    this.onTap,
    this.physic,
    this.file,
    this.fit,
    this.iconSize = 30,
  });

  @override
  Widget build(BuildContext context) {
    // print(widget.visibleDot);
    return InkWell(
      highlightColor: Colors.transparent,
      onTap: onTap == null ? null : onTap as void Function(),
      child: file != null
          ? AnimatedContainer(
              duration: Duration(milliseconds: 200),
              height: height,
              width: width ?? Get.width,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(.4),
                image: DecorationImage(
                  image: FileImage(file!),
                  fit: fit ?? BoxFit.cover,
                ),
                borderRadius: borderRadius ?? BorderRadius.zero,
              ),
            )
          : url == null || url!.isEmpty
              ? AnimatedContainer(
                  duration: Duration(milliseconds: 200),
                  height: height,
                  width: width ?? Get.width,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(.4),
                    borderRadius: borderRadius ?? BorderRadius.zero,
                  ),
                  child: Center(
                    child: Icon(
                      Icons.error,
                      size: iconSize,
                    ),
                  ),
                )
              : InkWell(
                  borderRadius: borderRadius ?? BorderRadius.zero,
                  onTap: onTap,
                  child: CachedNetworkImage(
                    imageUrl: url!,
                    imageBuilder: (context, imageProvider) {
                      return ClipRRect(
                        borderRadius: borderRadius ?? BorderRadius.zero,
                        child: Image(
                          height: height,
                          width: width,
                          image: imageProvider,
                          fit: fit ?? BoxFit.fill,
                        ),
                      );
                    },
                    errorWidget: (context, url, error) {
                      return AnimatedContainer(
                        duration: Duration(milliseconds: 200),
                        height: height,
                        alignment: Alignment.center,
                        width: width,
                        decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(.4),
                          borderRadius: borderRadius ?? BorderRadius.zero,
                        ),
                        child: Center(
                          child: Icon(
                            Icons.error,
                            size: iconSize,
                          ),
                        ),
                      );
                    },
                    progressIndicatorBuilder: (context, url, progress) {
                      return Shimmer.fromColors(
                        direction: ShimmerDirection.ttb,
                        highlightColor: Colors.grey.withOpacity(.6),
                        baseColor: Colors.grey.withOpacity(.3),
                        child: Container(
                          width: width,
                          height: height,
                          decoration: BoxDecoration(
                            borderRadius: borderRadius ?? BorderRadius.zero,
                            color: Colors.grey,
                          ),
                        ),
                        period: Duration(milliseconds: 800),
                      );
                    },
                  ),
                ),
    );
  }
}
