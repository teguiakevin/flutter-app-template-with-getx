import 'package:encrypt/encrypt.dart';

class EncryptData {
  final _key = Key.fromUtf8("r7lQqfxeKOcZxPVpjgqVt1GNmsPLw4vj");
  final _iv = IV.fromLength(16);

  String cryptData(String data) {
    final encrypter = Encrypter(AES(_key));
    final encrypted = encrypter.encrypt(data, iv: _iv);
    return encrypted.base64;
  }

  String decryptData(String data) {
    final encrypter = Encrypter(AES(_key));
    final decrypted = encrypter.decrypt(Encrypted.from64(data), iv: _iv);
    return decrypted;
  }
}
