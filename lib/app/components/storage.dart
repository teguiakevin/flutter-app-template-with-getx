import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'encrypt_data.dart';

class LocalStorage {
  SharedPreferences box = Get.find<SharedPreferences>();

  final Map<String, List<ValueSetter>> _listener = {};
  Future<void> write(String key, dynamic value) async {
    if (value == null) {
      box.setString(key, value);
      return;
    }

    await box.setString(key, EncryptData().cryptData(json.encode(value)));
    if (_listener[key] != null) {
      for (ValueSetter callback in _listener[key]!) {
        callback.call(value);
      }
    }
    return;
  }

  Future<void> remove(String key) async {
    await box.remove(key);
    return;
  }

  T? read<T>(String key) {
    final String? value = box.getString(key);
    if (value == null) {
      return null;
    }

    String? tmp = json.decode(EncryptData().decryptData(value));
    return tmp != null ? tmp as T? : value as T?;
  }

  Future<void> clear() async {
    await box.clear();
    return;
  }

  void listenKey(String key, ValueSetter callback) {
    if (_listener[key] == null) {
      _listener[key] = [callback];
    } else if (_listener[key]!.contains(callback)) {
      return;
    } else {
      _listener[key]!.add(callback);
    }
  }

  @visibleForTesting
  static void setMockInitialValues(Map<String, Object> values) {
    Map<String, String> encryptedValue =
        values.map((String key, Object value) => MapEntry(key, EncryptData().cryptData(json.encode(value))));
    // ignore: invalid_use_of_visible_for_testing_member
    SharedPreferences.setMockInitialValues(encryptedValue);
  }
}
