import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../configs/config.dart';
import '../widgets/ios_loading.dart';
import '../widgets/loader_widget.dart';

extension Utils on GetInterface {
  void showLoader({bool dismissible = false, bool canUserPop = false, bool dense: false}) {
    dialog(
        WillPopScope(
            onWillPop: () async {
              return canUserPop;
            },
            child: LoaderWidget(dense: dense)),
        barrierDismissible: dismissible);
  }

  void showLoader2({bool dismissible = false, bool canUserPop = false, bool dense: false}) {
    dialog(
      WillPopScope(
          onWillPop: () async {
            return canUserPop;
          },
          child: const Center(
            child: IosLoading(),
          )),
      barrierDismissible: dismissible,
      barrierColor: Colors.transparent,
    );
  }

  void closeLoader() {
    if (isDialogOpen!) {
      Get.back();
    }
  }

  void copy(String? value) {
    Clipboard.setData(ClipboardData(text: value));
    // showtoast(
    //   LocaleKeys.alert_asn_copied.tr,
    //   color: Config.colors.primaryColor,
    // );
  }

  Widget circularLoader() => Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Get.theme.primaryColor),
        ),
      );

  void showtoast(String msg, {Color color = Colors.red, ToastGravity position = ToastGravity.TOP}) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: position,
        timeInSecForIosWeb: 4,
        backgroundColor: color,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  Widget getDescriptionWidget(
    String value, {
    List<String>? arg,
    TextAlign? textAlign,
    double? fontSize1,
    double? fontSize2,
    Color? color1,
    Color? color2,
  }) {
    final data = value.split('%s');

    return RichText(
        textAlign: textAlign ?? TextAlign.center,
        text: TextSpan(children: [
          for (var i = 0; i < data.length; i++) ...[
            TextSpan(
                style: Get.textTheme.headline3!.copyWith(
                  color: color1,
                  fontSize: fontSize1 ?? 13,
                ),
                text: data[i]),
            TextSpan(
                style: Get.textTheme.headline4!.copyWith(color: color2 ?? Config.colors.primaryColor, fontSize: fontSize2 ?? 15),
                text: arg != null && arg.isNotEmpty && i < arg.length ? arg[i] : ''),
          ]
        ]));
  }

  Widget customRichText(
    String value, {
    List<String>? args,
    TextAlign? textAlign,
    TextStyle? style1,
    TextStyle? style2,
    final void Function(int value)? onTap,
  }) {
    final data = value.split("%s");

    return RichText(
        textAlign: textAlign ?? TextAlign.start,
        text: TextSpan(children: [
          for (var i = 0; i < data.length; i++) ...[
            TextSpan(
                style: style1 ??
                    Get.textTheme.headline3!.copyWith(
                      fontSize: Config.sizes.p10,
                    ),
                text: data[i]),
            WidgetSpan(
              alignment: PlaceholderAlignment.middle,
              child: InkWell(
                onTap: () {
                  if (args != null && args.isNotEmpty && i < args.length) {
                    onTap?.call(i);
                  }
                },
                child: Text(
                  args != null && args.isNotEmpty && i < args.length ? args[i] : '',
                  style: style2 ??
                      Get.textTheme.headline4!.copyWith(
                        color: Config.colors.primaryColor,
                        fontSize: Config.sizes.p10,
                      ),
                ),
              ),
            ),
          ]
        ]));
  }
}
