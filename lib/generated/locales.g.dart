// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

// ignore_for_file: lines_longer_than_80_chars
// ignore: avoid_classes_with_only_static_members
class AppTranslation {
  static Map<String, Map<String, String>> translations = {
    'en': Locales.en,
    'fr': Locales.fr,
  };
}

class LocaleKeys {
  LocaleKeys._();
  static const actions_confirm = 'actions_confirm';
  static const actions_validate = 'actions_validate';
  static const labels_name = 'labels_name';
  static const texts_onboarding_message_1 = 'texts_onboarding_message_1';
  static const errors_network_error = 'errors_network_error';
  static const alerts_signin_success = 'alerts_signin_success';
}

class Locales {
  static const en = {
    'actions_confirm': 'Confirm',
    'actions_validate': 'Validate',
    'labels_name': 'Name',
    'texts_onboarding_message_1': 'Cool app',
    'errors_network_error': 'Network issues',
    'alerts_signin_success': 'Sign In successfull',
  };
  static const fr = {
    'actions_confirm': 'Confirmer',
    'actions_validate': 'Valider',
    'labels_name': 'Nom',
    'texts_onboarding_message_1': 'Cool app',
    'errors_network_error': 'Erreur connexion internet',
    'alerts_signin_success': 'Login effectué avec succès',
  };
}
