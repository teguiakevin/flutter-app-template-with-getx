# Flutter template with GetX

This is my flutter template for all my project. To begin with this template you need to follow some steps

## Getting Started

- Clone this repository
- Install the awesome library [GetX](https://pub.dev/packages/get_cli), In fact we use GetX for the dependancy injection, custom routing, State management and localizations. but not that you can use your own state managment system but you will have to change it yourself
- Install the command line [get_cli](https://pub.dev/packages/get) to help you generate all the boilerplate code for localization, new modules, new pages, new controllers, new models, new api providers
- Now you must change the package name in AndroidManifest.xml and android/app/build.gradle and put your own package name (if you have and android version)
- Change the your project name in pubspecs.yml
- Update the lint options in analysis_options.yaml and put the one that suit you

## Understanding the project Structure

inside the lib directory you have :

- app directory where all the app will be define
- generated directory for all generate locales
- main.dart to start the app
- init.dart to inject all the dependancies in the dependancy container it's a part of main.dart directory

### The app directory
